/*
  - Refer ldr_atmega32_A0_microhope.jpg for circuit.
  - Use read_ldr_uart.py to check the values
 */
#include <mh/mh-utils.c>
#include <mh/mh-lcd.c>
#include <mh/mh-uart.c>
#include <mh/mh-adc.c>

int main(void) {

    uint8_t low, hi;
    uint16_t adcval;

    lcd_init();
    uart_init(38400);
    adc_enable();
    for(;;) {
	adcval = read_adc(0);	/* pin 40(A0) */

	lcd_clear();
	lcd_put_int(adcval);

	low = adcval & 255;
	hi = adcval >> 8;

	uart_send_byte(low);  // send LOW byte
	uart_send_byte(hi);  // send HI byte
	delay_ms(1000);
    }
}
