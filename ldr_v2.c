/*
  - Refer ldr_atmega32_A0_microhope.jpg for circuit.
  - Use read_ldr_uart.py to check the values
 */
#include <avr/io.h>
#include <mh/mh-utils.c>
#include <mh/mh-lcd.c>
#include <mh/mh-uart.c>


void adc_enable(void) {
    /*
      Enable the ADC
     */
    ADCSRA = (1 << ADEN);
    ADMUX = (1 << REFS0);		// Use AVCC as reference voltage
}

uint16_t read_a0(void) {
    uint8_t low;
    uint16_t val;

    ADMUX = (1 << REFS0) | (1 << MUX0);  // Use AVCC as reference OR AC0(pin 40)

      /*
	Enable the ADC | Start convertion | ADC clock prescaler /128
     */
    ADCSRA = (1 << ADEN) | (1 << ADSC) | (1 << ADPS0) | (1 << ADPS1) | (1 << ADPS2);
    while ( !(ADCSRA & (1<<ADIF)) ) ;	// wait for ADC conversion
    ADCSRA |= ADIF;
    low = ADCL;
    val = ADCH;
    return (val << 8) | low;
}

int main(void) {

    uint8_t low, hi;
    uint16_t adcval;
    DDRB = 0b00000001;		/* pin 0 */
    PORTB = 0x00;		/* Keep all LEDs off */

    lcd_init();
    uart_init(38400);
    adc_enable();
    for(;;) {
	adcval = read_a0();

	lcd_clear();
	lcd_put_int(adcval);
	if(adcval < 300) {	/* Set D2 to high if value is below 300  */
	    PORTB |= 1;
	}
	else {
	    PORTB &= ~1;
	}

	low = adcval & 255;
	hi = adcval >> 8;

	uart_send_byte(low);  // send LOW byte
	uart_send_byte(hi);  // send HI byte
	delay_ms(1000);
    }
}
